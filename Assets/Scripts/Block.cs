﻿using System;
using UnityEngine;

public class Block : MonoBehaviour
{
    public enum BlockTypes {
        Cyan,
        Orange,
        Magenta,
        Yellow,
        Green,
        Red,
        Black,
    }

    public bool isChecked;

    public int x;
    public int y;

    public BlockTypes blockType;

    // Start is called before the first frame update
    void Start()
    {
        isChecked = false;
        x = -1; y = -1;
    }

    public void MoveTo(Vector3 pos) {
        transform.position = pos;
    }
}
