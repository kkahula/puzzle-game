﻿using System;
using UnityEngine;
using Random = UnityEngine.Random;

public class GameManager : MonoBehaviour
{
    public GameObject[] blocks;
    public BoardMode boardMode = BoardMode.Normal;
    public GameObject player;
    [Range(1,7)] public int useBlocks;

    private GameBoard board;
    private GameState state;
    private PlayerController playerController;

    public enum BoardMode {
        Normal,
        TestPatternHorizontal,
        TestPatternSW,
        TestPatternVertical,
        TestPatternNW,
        TestPatternNE,
        TestPatternSE,
        TestPatternCross,
        TestPattern4x4,
        TestPatternRing,
        TestPatternBlock
    }

    public enum GameState {
        Ready,
        Init,
        Matching
    }

    // Start is called before the first frame update
    void Start()
    {
        state = GameState.Init;
        SetupBoard(boardMode);
        SetupPlayer();
        state = GameState.Ready;

    }

    private void SetupPlayer() {
        playerController = player.GetComponent<PlayerController>();
        playerController.w = GameBoard.w;
        playerController.Center();
    }

    private void SetupBoard(BoardMode boardMode) {
        if(board == null) {
            board = this.gameObject.AddComponent<GameBoard>();
        }

        board.Clear();

        switch(boardMode) {
            case (BoardMode.Normal):
                CreateRandomBoard();
                break;

            #region Test Pattern Horizontal
            case (BoardMode.TestPatternHorizontal):
                board.Place(blocks[0], 0, 0);
                board.Place(blocks[0], 1, 0);
                board.Place(blocks[0], 2, 0);
                board.Place(blocks[0], 3, 0);
                board.Place(blocks[0], 4, 0);
                board.Place(blocks[0], 5, 0);
                board.Place(blocks[0], 6, 0);

                board.Place(blocks[1], 0, 1);
                board.Place(blocks[1], 1, 1);
                board.Place(blocks[1], 2, 1);
                board.Place(blocks[1], 3, 1);
                board.Place(blocks[1], 4, 1);
                board.Place(blocks[1], 5, 1);

                board.Place(blocks[2], 0, 2);
                board.Place(blocks[2], 1, 2);
                board.Place(blocks[2], 2, 2);
                board.Place(blocks[2], 3, 2);
                board.Place(blocks[2], 4, 2);

                board.Place(blocks[3], 0, 3);
                board.Place(blocks[3], 1, 3);
                board.Place(blocks[3], 2, 3);
                board.Place(blocks[3], 3, 3);

                board.Place(blocks[4], 0, 4);
                board.Place(blocks[4], 1, 4);
                board.Place(blocks[4], 2, 4);

                board.Place(blocks[5], 0, 5);
                board.Place(blocks[5], 1, 5);

                board.Place(blocks[6], 0, 6);
                break;
            #endregion
            #region Test Pattern Vertical
            case (BoardMode.TestPatternVertical):
                board.Place(blocks[0], 0, 0);
                board.Place(blocks[6], 0, 1);

                board.Place(blocks[1], 1, 0);
                board.Place(blocks[1], 1, 1);
                board.Place(blocks[6], 1, 2);

                board.Place(blocks[2], 2, 0);
                board.Place(blocks[2], 2, 1);
                board.Place(blocks[2], 2, 2);
                board.Place(blocks[6], 2, 3);

                board.Place(blocks[3], 3, 0);
                board.Place(blocks[3], 3, 1);
                board.Place(blocks[3], 3, 2);
                board.Place(blocks[3], 3, 3);
                board.Place(blocks[6], 3, 4);

                board.Place(blocks[4], 4, 0);
                board.Place(blocks[4], 4, 1);
                board.Place(blocks[4], 4, 2);
                board.Place(blocks[4], 4, 3);
                board.Place(blocks[4], 4, 4);
                board.Place(blocks[6], 4, 5);

                board.Place(blocks[5], 5, 0);
                board.Place(blocks[5], 5, 1);
                board.Place(blocks[5], 5, 2);
                board.Place(blocks[5], 5, 3);
                board.Place(blocks[5], 5, 4);
                board.Place(blocks[5], 5, 5);
                board.Place(blocks[6], 5, 6);

                board.Place(blocks[6], 6, 0);
                board.Place(blocks[6], 6, 1);
                board.Place(blocks[6], 6, 2);
                board.Place(blocks[6], 6, 3);
                board.Place(blocks[6], 6, 4);
                board.Place(blocks[6], 6, 5);
                board.Place(blocks[6], 6, 6);
                break;
            #endregion
            case (BoardMode.TestPatternSW):
                board.Place(blocks[1], 0, 0);
                board.Place(blocks[2], 1, 0);
                board.Place(blocks[3], 2, 0);
                board.Place(blocks[4], 0, 1);

                board.Place(blocks[5], 1, 1);
                board.Place(blocks[5], 2, 1);
                board.Place(blocks[5], 1, 2);
                break;
            case (BoardMode.TestPatternNW):
                board.Place(blocks[0], 0, 0);
                board.Place(blocks[1], 1, 0);
                board.Place(blocks[0], 0, 1);
                board.Place(blocks[0], 1, 1);
                break;
            case (BoardMode.TestPatternNE):
                board.Place(blocks[0], 0, 0);
                board.Place(blocks[1], 1, 0);
                board.Place(blocks[1], 0, 1);
                board.Place(blocks[1], 1, 1);
                break;
            case (BoardMode.TestPatternSE):
                board.Place(blocks[0], 0, 0);
                board.Place(blocks[0], 1, 0);
                board.Place(blocks[1], 0, 1);
                board.Place(blocks[0], 1, 1);
                break;
            case (BoardMode.TestPatternCross):
                board.Place(blocks[0], 0, 0);
                board.Place(blocks[1], 1, 0);
                board.Place(blocks[0], 2, 0);
                board.Place(blocks[1], 0, 1);
                board.Place(blocks[1], 1, 1);
                board.Place(blocks[1], 2, 1);
                board.Place(blocks[0], 0, 2);
                board.Place(blocks[1], 1, 2);
                board.Place(blocks[0], 2, 2);
                break;
            case (BoardMode.TestPattern4x4):
                board.Place(blocks[0], 0, 0);
                board.Place(blocks[0], 1, 0);
                board.Place(blocks[0], 0, 1);
                board.Place(blocks[0], 1, 1);
                break;
            case (BoardMode.TestPatternBlock):
                board.Place(blocks[0], 0, 0);
                board.Place(blocks[0], 1, 0);
                board.Place(blocks[0], 2, 0);
                board.Place(blocks[0], 0, 1);
                board.Place(blocks[0], 1, 1);
                board.Place(blocks[0], 2, 1);
                board.Place(blocks[0], 0, 2);
                board.Place(blocks[0], 1, 2);
                board.Place(blocks[0], 2, 2);
                break;
            case (BoardMode.TestPatternRing):
                board.Place(blocks[0], 0, 0);
                board.Place(blocks[0], 1, 0);
                board.Place(blocks[0], 2, 0);
                board.Place(blocks[0], 0, 1);
                board.Place(blocks[1], 1, 1);
                board.Place(blocks[0], 2, 1);
                board.Place(blocks[0], 0, 2);
                board.Place(blocks[0], 1, 2);
                board.Place(blocks[0], 2, 2);
                break;
        }
    }

    private void CreateRandomBoard() {
        int brickQuota = (int) Math.Floor(GameBoard.w * GameBoard.h * 0.4);
        int x, y;

        while (brickQuota > 0) {
            do {
                x = Random.Range(0, max: GameBoard.w);
                y = Random.Range(0, max: GameBoard.h - 1);
            } while (!board.Place(GetBlock(), x, y));
            brickQuota--;
        }
        board.Settle();
    }

    private GameObject GetBlock() {
        return blocks[Random.Range(0, max: useBlocks)];
    }

    void Update()
    {
        if (state == GameState.Ready) {
            board.Settle();

            Vector2 directionalInputRaw = new Vector2(Input.GetAxisRaw("Horizontal"), Input.GetAxisRaw("Vertical"));
            if(directionalInputRaw.y > 0) {
                playerController.TakeBlock();
            }
            if(directionalInputRaw.y < 0) {
                playerController.DropBlocks();
            }
            if(directionalInputRaw.x < 0) {
                playerController.MoveLeft();
            }
            if(directionalInputRaw.x > 0) {
                playerController.MoveRight();
            }
        }
    }
}
