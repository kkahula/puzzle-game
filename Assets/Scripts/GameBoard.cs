﻿using System;
using System.Collections.Generic;
using UnityEngine;

public class GameBoard : MonoBehaviour
{
    public static int w = 7;
    public static int h = 13; // anything in 13 is game over
    public static Block[,] grid = new Block[w, h];
    private static readonly int matchThreshold = 3;
    List<Block> matchedBlocks;

    private void Start() {
        matchedBlocks = new List<Block>();
    }

    internal bool Place(GameObject p, int x, int y) {
        if(p == null) {
            Destroy(grid[x, y]);
        }
        else {
            if(grid[x, y] != null) {
                return false;
            }

            var newBlock = Instantiate(p, CalculateGridPos(x, y), Quaternion.identity).GetComponent<Block>();
            grid[x, y] = newBlock;
        }
        return true;
    }

    internal void Clear() {
        for (int x=0; x < w; x++) {
            for (int y=0; y < h; y++) {
                Place(null, x, y);
            }
        }
    }

    internal void Settle() {
        for(int y = 0; y < h; y++) {
            for(int x = 0; x < w; x++) {
                SettleBlock(x, y);
            }
        }
    }

    private void SettleBlock(int x, int y) {
        int oldY = y;
        if(grid[x,y] != null) {
            int nextY = y - 1;

            while(nextY >= 0 && grid[x, nextY] == null) {
                y--; nextY--;
            }
            MoveFromTo(x, oldY, x, y);
        }
    }

    private void MoveFromTo(int fromX, int fromY, int toX, int toY) {
        if(grid[fromX, fromY] != null && grid[toX, toY] == null) {
            Block block = grid[fromX, fromY];
            grid[fromX, fromY] = null;
            grid[toX, toY] = block;
            block.MoveTo(CalculateGridPos(toX, toY));
        }
    }

    private Vector3 CalculateGridPos(int x, int y) {
        float v = 0.25F;
        return new Vector3(x - (w / 2), 1 + y - v - (h / 2));
    }

    internal void Match() {
        UncheckBlocks();
        matchedBlocks = new List<Block>();
        for (int y = 0; y < h; y++) {
            for(int x = 0; x < w; x++) {
                Block block = grid[x, y];
                if(block != null && !block.isChecked) {
                    List<Block> matches = CheckBlock(x, y, block);
                    if (matches.Count >= matchThreshold) {
                        matchedBlocks.AddRange(matches);
                    }
                }
            }
        }
        matchedBlocks.ForEach(delegate (Block b) { Destroy(b.gameObject); });
    }

    private List<Block> CheckBlock(int x, int y, Block previousBlock) {
        List<Block> matches = new List<Block>();
        if(x >= 0 && x < w && y >= 0 && y < h) {
            Block thisBlock = grid[x, y];
            if(thisBlock != null && !thisBlock.isChecked && previousBlock.blockType == thisBlock.blockType) {
                thisBlock.isChecked = true;
                matches.Add(thisBlock);
                matches.AddRange(CheckBlock(x + 1, y, thisBlock)); // right
                matches.AddRange(CheckBlock(x, y + 1, thisBlock)); // up
                matches.AddRange(CheckBlock(x - 1, y, thisBlock)); // left
            }
        }

        return matches;
    }

    private void UncheckBlocks() {
        for(int x = 0; x < w; x++) {
            for (int y = 0; y < h; y++) {
                if(grid[x, y] != null) {
                    grid[x, y].isChecked = false;
                }
            }
        }
    }
}
