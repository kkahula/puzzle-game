using System;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    public int w;
    public int gridPosition;
    [Range(1, 5)] public int maxBlocks;

    private List<Block> blocks;

    private void Start() {
        blocks = new List<Block>();
    }

    public void TakeBlock() {

    }

    public void DropBlocks() {

    }

    public void MoveLeft() {
        Move(gridPosition-1);
    }

    public void MoveRight() {
        Move(gridPosition+1);
    }

    public void Center() {
        int center = w % 2 == 0 ? w / 2 : w / 2 + 1;
        Move(center);
    }

    private void Move(int x) {
        if(x >= 0 && x < w) {
            gridPosition = x;
            transform.position = new Vector3(
                gridPosition - (w / 2),
                transform.position.y,
                transform.position.z);
        }
    }
}